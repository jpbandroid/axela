# Axela Changelog
This is a list of changes that were added in every public version of Axela
## v1.0, October 31, 2021
<i>Initial release</i>
## v1.1, April 12, 2022
Axela can now talk to you!<br>Axela can now retrieve content from Wikipedia articles, expanding the Axela knowledge base!
## v1.2, September 9, 2022
Axela can now answer all simple mathematical calculations. Just type <code>Axela, calculate</code> and enter the calculation into the input fields
