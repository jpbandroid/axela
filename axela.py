import tkinter as tk
from tkinter import ttk
from tkinter.messagebox import showerror, showwarning, showinfo, askyesno
from tkinter import WORD
from datetime import datetime
import pyttsx3
import wikipedia
import webbrowser
import platform
engine = pyttsx3.init()
voices = engine.getProperty('voices')
engine.setProperty('voice', voices[2].id)
def axelaprocess():
    cmd = text.get()
    if cmd == "Axela, meow":
        ans = "meow \U0001F638"
        msg =  ans
        engine.say(msg)
        engine.runAndWait()
        showinfo(
            title='Answer',
            message=msg
    )
    elif cmd == "Axela, say hello":
        ans = "Hello!"
        msg =  ans
        engine.say(msg)
        engine.runAndWait()
        showinfo(
            title='Answer',
            message=msg
    )
    elif cmd == "Axela, see you tomorrow":
        import getpass
        user = getpass.getuser()
        ans = "See you tomorrow, "
        msg =  ans + user
        engine.say(msg)
        engine.runAndWait()
        showinfo(
            title='Answer',
            message=msg
    )
    elif cmd == "Axela, what's the time?":
        now = str(datetime.now())
        ans = "The time is: "
        msg =  ans + now
        engine.say(msg)
        engine.runAndWait()
        showinfo(
            title='Answer',
            message=msg
    )
    elif cmd == "Axela, woof":
        ans = "woof \U0001F436"
        msg =  ans
        showinfo(
            title='Answer',
            message=msg
    )
        engine.say(msg)
        engine.runAndWait()
    elif cmd == "Axela, who are you?":
        ans = "I am an AI. This means that I take your question and put it into an answer. Plus, I do it on your computer, not on a server! \U0001F5A5 /  \U0001F4BB"
        msg =  ans
        showinfo(
            title='Answer',
            message=msg
    )
        engine.say(msg)
        engine.runAndWait()
    elif cmd == "Axela, Axela":
        ans = "Hi there! Nice to see you! \U0001F44B"
        msg =  ans
        engine.say(msg)
        engine.runAndWait()
        showinfo(
            title='Answer',
            message=msg
            )
    elif 'Axela, search wikipedia' in cmd:
            engine.say('Searching Wikipedia...')
            cmd = cmd.replace("Axela, search wikipedia", "")
            results = wikipedia.summary(cmd, sentences=3, auto_suggest=False)
            engine.say("Some Wikipedia articles can be vandalized. Be careful, as everything in these articles may not be true, or may be hurtful to some people.")
            engine.say("According to Wikipedia")
            engine.say(results)
            engine.runAndWait()
            showinfo(
            title='Results from Wikipedia',
            message=results
            )
    elif 'wikipedia' in cmd:
            engine.say('Searching Wikipedia...')
            cmd = cmd.replace("wikipedia", "")
            results = wikipedia.summary(cmd, sentences=3, auto_suggest=False)
            engine.say("Some Wikipedia articles can be vandalized. Be careful, as everything in these articles may not be true, or may be hurtful to some people.")
            engine.say("According to Wikipedia")
            engine.say(results)
            engine.runAndWait()
            showinfo(
            title='Results from Wikipedia',
            message=results
            )
    elif 'open youtube' in cmd:
            webbrowser.open_new_tab("https://www.youtube.com")
            engine.say("youtube is open now")
            engine.runAndWait()
            time.sleep(5)

    elif 'open google' in cmd:
        webbrowser.open_new_tab("https://www.google.com")
        engine.say("Google Search is open now")
        engine.runAndWait()
        time.sleep(5)

    elif 'open gmail' in cmd:
        webbrowser.open_new_tab("gmail.com")
        engine.say("Gmail is open now")
        engine.runAndWait()
        time.sleep(5)
    elif cmd == "Axela, what is my Windows version?":
        engine.say('Your Windows version is' + platform.version())
        engine.runAndWait()
        showinfo(
              title="Windows Version",
              message=platform.version()
            )
    elif "Axela, calculate" in cmd:
        engine.say("please type the calculation you want me to calculate into the textboxes")
        engine.runAndWait()
        arithmeticwindow()
    else:
        ans = "I haven't been programmed to give you an answer to that yet... Sorry! Maybe next time!"
        msg =  ans
        engine.say(msg)
        engine.runAndWait()
        showinfo(
            title='Answer',
            message=msg
            )
#some code for installer to build
def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)
def arithmeticwindow():
    arithmeticwindow = tk.Tk()
    arithmeticwindow.title("Calcuation")
    number1SV = tk.StringVar()
    number2SV = tk.StringVar()
    label1 = ttk.Label(arithmeticwindow, text="What calculation do you want Axela to work out?")
    label1.pack()
    entry1 = ttk.Entry(arithmeticwindow, textvariable=number1SV)
    entryval = ttk.Combobox(arithmeticwindow, state='readonly')
    entryval['values'] = ('+', '-', '×', '÷')
    entry2 = ttk.Entry(arithmeticwindow, textvariable=number2SV)
    number1 = entry1.get()
    number2 = entry2.get()
    def numprocess():
        if entryval.get() == '+':
            ans = int(entry1.get()) + int(entry2.get())
        elif entryval.get() == '-':
            ans = int(entry1.get()) + int(entry2.get())
        elif entryval.get() == '×':
            ans = int(entry1.get()) * int(entry2.get())
        elif entryval.get() == '÷':
            ans = int(entry1.get()) / int(entry2.get())
        else:
            ans = 0
        msg = str(ans)
        engine.say(msg)
        engine.runAndWait()
        showinfo(
            title='Answer',
            message=msg
            )
    confirmbutton = ttk.Button(arithmeticwindow, text="Calculate", command=numprocess)
    entry1.pack()
    entryval.pack()
    entry2.pack()
    confirmbutton.pack()
def about():
    aboutwindow = tk.Tk()
    aboutwindow.title("About Axela")
    aboutlabel = ttk.Label(aboutwindow, text="Axela 1.2 dev build 1207", font=("System", 24))
    aboutlabel.pack()
    aboutlabel2 = ttk.Label(aboutwindow, text="released 11/9/2022", font=("System", 16))
    aboutlabel2.pack()
    aboutlabel3 = ttk.Label(aboutwindow, text="PRIVACY NOTICE\nAxela uses your device's hardware, not sending the data to servers, in generating the responeses. They are all in the source code of the AI, by the way!!", font=("System", 16), wraplength=300, justify="center")
    aboutlabel3.pack()
    aboutwindow.mainloop()
def welcome():
    welcome = tk.Tk()
    welcome.title("Welcome!")
    engine.say("Axela welcomes you to an experience that you've never seen before...")
    engine.runAndWait()
    aboutlabel = ttk.Label(welcome, text="Axela welcomes you to an experience that you've never seen before...", font=("System", 24))
    aboutlabel.pack()
    aboutlabel3 = ttk.Label(welcome, text="An AI that runs on your PC (NO SERVERS!), to help you every day. No false answers, 100% free and open-source, works offline*, and respecting your privacy.\nAI reimagined... ...the Axela era starts now!\n*Wikipedia article search function does not work offline.", font=("System", 16), wraplength=300, justify="center")
    aboutlabel3.pack()
    welcome.mainloop()
def exit():
    answer = askyesno(title='Exit',
                    message='Are you sure that you want to quit?')
    if answer:
        root.destroy()
root = tk.Tk(className="axela")
root.title('Axela')
root.iconbitmap('axelaico.ico')
# create a menubar
menubar = tk.Menu(root)
root.config(menu=menubar)

# create a menu
file_menu = tk.Menu(menubar, tearoff=False)

# add a menu item to the menu
file_menu.add_command(
    label='Exit',
    command=exit
)

# add the File menu to the menubar
menubar.add_cascade(
    label="File",
    menu=file_menu
)
# create the Help menu
help_menu = tk.Menu(
    menubar,
    tearoff=0
)

help_menu.add_command(label='Welcome', command=welcome)
help_menu.add_command(label='About...', command=about)

# add the Help menu to the menubar
menubar.add_cascade(
    label="Help",
    menu=help_menu
)

img = ttk.Label(root,  text="What do you want to ask today?", compound='left')
img.pack()
text = tk.StringVar()
textbox = ttk.Entry(root, textvariable=text)
textbox.pack(expand=True)
login_button = ttk.Button(root, text="Ask Axela!", command=axelaprocess)
login_button.pack(expand=True)
textbox.focus()
# Pack it
root.mainloop() 
